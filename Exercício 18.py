n1 = float(input('Digite a 1º primeira nota: '))
n2 = float(input('Digite a 2º nota: '))
n_opt = float(input('Digite a nota optativa (caso tenha, senão digite -1): '))

if n_opt == -1:
    media = (n1 + n2)/2
elif n1 < n2:
    media = (n2 + n_opt)/2
else:
    media = (n1 + n_opt)/2
if media >= 6:
    print(f'Aprovado com {media}.')
elif media >= 3:
    print(f'Aluno em exame com {media}.')
else:
    print(f'Reprovado com {media}.')







